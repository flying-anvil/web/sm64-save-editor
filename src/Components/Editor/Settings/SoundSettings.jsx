// @flow
import React from 'react';
import {Col, Form, Row} from "react-bootstrap";

type Props = {
  value: number,
  update: Function,
};

export default function SoundSettings(props: Props) {
  const {value, update} = props;

  const onChange = (event) => {
    const newValue = parseInt(event.target.value);
    update(newValue);
  }

  return (
    <Row>
      <Col md={4}>
        <Form.Group controlId="exampleForm.ControlSelect1">
          <Form.Label>Sound</Form.Label>
          <Form.Control as="select" value={value} onChange={onChange} custom>
            <option value={0x00}>0x00: Stereo</option>
            <option value={0x01}>0x01: Mono</option>
            <option value={0x02}>0x02: Headset</option>
          </Form.Control>
        </Form.Group>
      </Col>
    </Row>
  );
}
