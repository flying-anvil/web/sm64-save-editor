// @flow
import React from 'react';
import {Button, Tab, Tabs} from "react-bootstrap";
import Settings from "./Settings/Settings";
import saveImmerReducer from "../../Data/saveImmerReducer";
import {emptySave} from "../../Data/Save";
import Savegame from "./Savegame/Savegame";
import {useImmerReducer} from "use-immer";
import {FaRegSave} from "react-icons/all";
import {exportSave} from "../../Helper/SaveExporter";

type Props = {};

export default function Editor(props: Props) {
  const [saveData, updateSaveData] = useImmerReducer(saveImmerReducer, emptySave);

  return (
    <div className={'mb-3'}>
      <Tabs defaultActiveKey={'save-a'} id={'save-section'}>
        {['a', 'b', 'c', 'd'].map((slot) =>
          <Tab title={`Save ${slot.toUpperCase()}`} eventKey={`save-${slot}`} key={slot} className={'mb-4'}>
            <Savegame slot={slot} slotData={saveData.saveData[slot]} updateSaveData={updateSaveData}/>
          </Tab>
        )}
        <Tab title={'Settings'} eventKey={'settings'}><Settings globalData={saveData.globalData} updateSaveData={updateSaveData}/></Tab>
      </Tabs>
      <Button variant={'success'} onClick={() => {exportSave(saveData)}}>
        Export <FaRegSave/>
      </Button>
      <Button variant={'warning'} onClick={() => {exportSave(saveData, true)}} className={'ml-2'}>
        Export + Download <FaRegSave/>
      </Button>
    </div>
  );
}
