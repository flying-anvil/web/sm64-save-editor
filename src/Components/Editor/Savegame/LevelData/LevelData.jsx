// @flow
import React from 'react';
import type {HighscoresType, LevelsDataType} from "../../../../Data/Save";
import {Col} from "react-bootstrap";
import SingleLevelData from "./SingleLevelData";
import {courses} from "../../../../Data/Course";

type Props = {
  levelData: LevelsDataType,
  highscores: HighscoresType,
  updateSaveData: Function,
};

export default function LevelData(props: Props) {
  const {levelData, highscores, updateSaveData} = props;

  return (
    <div className={'mt-2'}>
      <Col md={10}>
        {courses.map((identifier) =>
          <React.Fragment key={identifier}>
            <SingleLevelData
              identifier={identifier}
              levelData={levelData[identifier]}
              highscore={highscores[identifier]}
              updateSaveData={updateSaveData}

            />
            <hr className={'mb-2 mt-2'}/>
          </React.Fragment>
        )}
      </Col>
    </div>
  );
}
