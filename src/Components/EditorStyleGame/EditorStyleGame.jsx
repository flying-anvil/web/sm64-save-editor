// @flow
import React from 'react';
import {useImmerReducer} from "use-immer";
import saveImmerReducer from "../../Data/saveImmerReducer";
import {emptySave} from "../../Data/Save";
import '../../Style/EditorStyleGame.css';
import imageMenuMain from "../../Resources/Images/EditorStyleGame/Menu/Main.png";

type Props = {};

export default function EditorStyleGame(props: Props) {
  const [saveData, updateSaveData] = useImmerReducer(saveImmerReducer, emptySave);

  const style = {backgroundImage: `url(${imageMenuMain})`, backgroundColor: '#0f0', width: '100%', height: '90vh'};
  const styleBox = {backgroundColor: '#aaa', width: '150px', height: '115px', position: 'absolute'}

  return (
    <div className={'es-game-menu es-game-menu-main'} style={style}>
      <div style={{...styleBox, top: '25%', left: '15%'}}>A</div>
      <div style={{...styleBox, top: '25%', left: '51%'}}>B</div>
      <div style={{...styleBox, top: '45%', left: '15%'}}>C</div>
      <div style={{...styleBox, top: '45%', left: '51%'}}>D</div>

      <div style={{...styleBox, top: '70%', left: '15%'}}>Score</div>
      <div style={{...styleBox, top: '70%', left: '35%'}}>Copy</div>
      <div style={{...styleBox, top: '70%', left: '55%'}}>Erase</div>
      <div style={{...styleBox, top: '70%', left: '75%'}}>Settings</div>
    </div>
  );
}
