// @flow

export const templateTranslation = {
  stars: {
    castle: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    course1: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    course2: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    course3: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    course4: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    course5: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    course6: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    course7: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    course8: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    course9: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    course10: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    course11: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    course12: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    course13: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    course14: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    course15: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    palaceWing: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    palaceMetal: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    palaceVanish: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    bowser1: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    bowser2: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    bowser3: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    secretSlide: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    secretWing: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    secretAquarium: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
    endCake: {
      star1: 'Star 1',
      star2: 'Star 2',
      star3: 'Star 3',
      star4: 'Star 4',
      star5: 'Star 5',
      star6: 'Star 6',
      starBonus: '100 Coin Star',
    },
  },
  levels: {
    castle:         'Castle',
    course1:        'Bob-Omb Battlefield',
    course2:        'Whomp\'s Fortress',
    course3:        'Jolly Roger Bay',
    course4:        'Cool, Cool Mountain',
    course5:        'Big Boo\'s Haunt',
    course6:        'Hazy Maze Cave',
    course7:        'Lethal Lava Land',
    course8:        'Shifting Sand Land',
    course9:        'Dire, Dire Docks',
    course10:       'Snowman\'s Land',
    course11:       'Wet-Dry World',
    course12:       'Tall, Tall Mountain',
    course13:       'Tiny-Huge Island',
    course14:       'Tick Tock Clock',
    course15:       'Rainbow Ride',
    palaceWing:     'Tower of the Wing Cap',
    palaceMetal:    'Cavern of the Metal Cap',
    palaceVanish:   'Vanish Cap Under the Moat',
    bowser1:        'Bowser in the Dark World',
    bowser2:        'Bowser in the Fire Sea',
    bowser3:        'Bowser in the Sky',
    secretSlide:    'The Princess\'s Secret Slide',
    secretWing:     'Wing Mario Over the Rainbow',
    secretAquarium: 'The Secret Aquarium',
    endCake:        'The End',
  }
}
