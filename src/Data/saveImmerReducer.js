// @flow

import type {LevelDataType, SaveSlotKeyType, SaveSlotType, SaveType} from "./Save";
import {clamp} from "../Helper/Math";
import {flags} from "./Flags";

type ActionType = {
  type: 'global' | 'flag' | 'star' | 'highscore' | 'cannon' | 'multiStar',
  data: Object,
};

type GlobalActionType = {
  setting: 'sound' | 'language',
  value: number,
}

type FlagActionType = {
  slot: SaveSlotKeyType,
  flag: string,
  value: boolean,
}

type StarActionType = {
  slot: SaveSlotKeyType,
  level: string,
  star: string,
  value: boolean,
}

type CannonActionType = {
  slot: SaveSlotKeyType,
  level: string,
  value: boolean,
}

type MultiStartActionType = {
  slot: SaveSlotKeyType,
  level: string,
  stars: {
    star1: boolean,
    star2: boolean,
    star3: boolean,
    star4: boolean,
    star5: boolean,
    star6: boolean,
    starBonus: boolean,
  }
}

function applyGlobal(state: SaveType, data: GlobalActionType) {
  const {setting, value} = data;

  state.globalData[setting] = value;
}

function applyFlag(state: SaveType, data: FlagActionType) {
  const {slot, flag, value} = data;

  const saveSlot: SaveSlotType = state.saveData[slot];
  saveSlot.flags[flag] = value;

  // TODO: filter, only if it's really in use
  if (flag !== flags.fileInUse) {
    saveSlot.flags.fileInUse = true;
  }
}

function applyStar(state: SaveType, data: StarActionType) {
  const {slot, level, star, value} = data

  const saveSlot: SaveSlotType = state.saveData[slot];
  const levelData: LevelDataType = saveSlot.levelData[level];
  levelData.stars[star] = value;

  // TODO: filter, only if it's really in use
  saveSlot.flags.fileInUse = true;
}

function applyHighscore(state: SaveType, data: StarActionType) {
  const {slot, level, value} = data

  const saveSlot: SaveSlotType = state.saveData[slot];
  saveSlot.highscores[level] = clamp(value, 0, 255);

  // TODO: filter, only if it's really in use
  saveSlot.flags.fileInUse = true;
}

function applyCannon(state: SaveType, data: CannonActionType) {
  const {slot, level, value} = data;

  const saveSlot: SaveSlotType = state.saveData[slot];
  const levelData: LevelDataType = saveSlot.levelData[level];
  levelData.cannonUnlocked = value;

  // TODO: filter, only if it's really in use
  saveSlot.flags.fileInUse = true;
}

function applyMultistar(state: SaveType, data: MultiStartActionType) {
  const {slot, level, stars} = data;

  const saveSlot: SaveSlotType = state.saveData[slot];
  const levelData: LevelDataType = saveSlot.levelData[level];

  levelData.stars = stars;
}

export default function saveImmerReducer(state: SaveType, action: ActionType) {
  const {type, data} = action;

  switch (type) {
    case 'global':
      applyGlobal(state, data);
      break;
    case 'flag':
      applyFlag(state, data);
      break;
    case 'star':
      applyStar(state, data);
      break;
    case 'highscore':
      applyHighscore(state, data);
      break;
    case 'cannon':
      applyCannon(state, data);
      break;
    case 'multiStar':
      applyMultistar(state, data);
      break;
    default:
      throw new Error(`Unknown type "${type}"`);
  }
}
