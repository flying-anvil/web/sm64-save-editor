// @flow

export type GlobalDataType = {
  sound:    0 | 1 | 2, // 'Stereo'  | 'Mono'   | 'Headset',
  language: 0 | 1 | 2, // 'English' | 'French' | 'German',
}

export type SaveFlagsType = {
  fileInUse: boolean,
  unlockedWingCap: boolean,
  unlockedMetalCap: boolean,
  unlockedVanishCap: boolean,
  hasKeyBasement: boolean,
  hasKeyUpstairs: boolean,
  keyDoorUnlockedBasement: boolean,
  keyDoorUnlockedUpstairs: boolean,
  direDireDocksPortalMoved: boolean,
  moatDrained: boolean,
  starDoorUnlockedSecretSlide: boolean,
  starDoorUnlockedWhompsFortress: boolean,
  starDoorUnlockedCoolCoolMountain: boolean,
  starDoorUnlockedJollyRogerBay: boolean,
  starDoorUnlockedBowserDarkWorld: boolean,
  starDoorUnlockedBowserFireSea: boolean,
  lostCapByLevelId: boolean,
  lostCapInShiftingSandLand: boolean,
  lostCapTallTallMountain: boolean,
  lostCapSnowmansLand: boolean,
  starDoorUnlocked3rdFloor: boolean,
  unused1: boolean,
  unused2: boolean,
  unused3: boolean,
}

export type LevelDataType = {
  stars: {
    star1: boolean,
    star2: boolean,
    star3: boolean,
    star4: boolean,
    star5: boolean,
    star6: boolean,
    starBonus: boolean,
  },
  cannonUnlocked: boolean,
}

export type LevelsDataType = {
  castle: LevelDataType,

  course1: LevelDataType,
  course2: LevelDataType,
  course3: LevelDataType,
  course4: LevelDataType,
  course5: LevelDataType,
  course6: LevelDataType,
  course7: LevelDataType,
  course8: LevelDataType,
  course9: LevelDataType,
  course10: LevelDataType,
  course11: LevelDataType,
  course12: LevelDataType,
  course13: LevelDataType,
  course14: LevelDataType,
  course15: LevelDataType,

  palaceWing: LevelDataType,
  palaceMetal: LevelDataType,
  palaceVanish: LevelDataType,

  bowser1: LevelDataType,
  bowser2: LevelDataType,
  bowser3: LevelDataType,

  secretSlide: LevelDataType,
  secretWing: LevelDataType, // Wing Mario Over the Rainbow
  secretAquarium: LevelDataType,

  endCake: LevelDataType,
}

export type HighscoresType = {
  course1: number,
  course2: number,
  course3: number,
  course4: number,
  course5: number,
  course6: number,
  course7: number,
  course8: number,
  course9: number,
  course10: number,
  course11: number,
  course12: number,
  course13: number,
  course14: number,
  course15: number,
}

export type SaveSlotType = {
  flags: SaveFlagsType,
  levelData: LevelsDataType,
  highscores: HighscoresType,
}

export type SaveSlotKeyType = 'a' | 'b' | 'c' | 'd';

export type SaveDataType = {
  [SaveSlotKeyType]: SaveSlotType,
}

export type SaveType = {
  globalData: GlobalDataType,
  saveData: SaveDataType,
}

// =============================================================================

const emptyLevelData: LevelDataType = {
  stars: {
    star1: false,
    star2: false,
    star3: false,
    star4: false,
    star5: false,
    star6: false,
    starBonus: false,
  },
  cannonUnlocked: false,
}

const emptySaveSlot: SaveSlotType = {
  flags: {
    fileInUse: false,
    unlockedWingCap: false,
    unlockedMetalCap: false,
    unlockedVanishCap: false,
    hasKeyBasement: false,
    hasKeyUpstairs: false,
    keyDoorUnlockedBasement: false,
    keyDoorUnlockedUpstairs: false,
    direDireDocksPortalMoved: false,
    moatDrained: false,
    starDoorUnlockedSecretSlide: false,
    starDoorUnlockedWhompsFortress: false,
    starDoorUnlockedCoolCoolMountain: false,
    starDoorUnlockedJollyRogerBay: false,
    starDoorUnlockedBowserDarkWorld: false,
    starDoorUnlockedBowserFireSea: false,
    lostCapByLevelId: false,
    lostCapInShiftingSandLand: false,
    lostCapTallTallMountain: false,
    lostCapSnowmansLand: false,
    starDoorUnlocked3rdFloor: false,
    unused1: false,
    unused2: false,
    unused3: false,
  },
  levelData: {
    castle: {...emptyLevelData},

    course1: {...emptyLevelData},
    course2: {...emptyLevelData},
    course3: {...emptyLevelData},
    course4: {...emptyLevelData},
    course5: {...emptyLevelData},
    course6: {...emptyLevelData},
    course7: {...emptyLevelData},
    course8: {...emptyLevelData},
    course9: {...emptyLevelData},
    course10: {...emptyLevelData},
    course11: {...emptyLevelData},
    course12: {...emptyLevelData},
    course13: {...emptyLevelData},
    course14: {...emptyLevelData},
    course15: {...emptyLevelData},

    bowser1: {...emptyLevelData},
    bowser2: {...emptyLevelData},
    bowser3: {...emptyLevelData},

    palaceWing: {...emptyLevelData},
    palaceMetal: {...emptyLevelData},
    palaceVanish: {...emptyLevelData},

    secretSlide: {...emptyLevelData},
    secretWing: {...emptyLevelData},
    secretAquarium: {...emptyLevelData},

    endCake: {...emptyLevelData},
  },
  highscores: {
    course1:  0,
    course2:  0,
    course3:  0,
    course4:  0,
    course5:  0,
    course6:  0,
    course7:  0,
    course8:  0,
    course9:  0,
    course10: 0,
    course11: 0,
    course12: 0,
    course13: 0,
    course14: 0,
    course15: 0,
  },
}

export const emptySave: SaveType = {
  globalData: {
    language: 0,
    sound: 0,
  },
  saveData: {
    a: {...emptySaveSlot},
    b: {...emptySaveSlot},
    c: {...emptySaveSlot},
    d: {...emptySaveSlot},
  }
}
