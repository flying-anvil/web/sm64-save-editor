// @flow

export const courses = [
  'castle',
  'course1',
  'course2',
  'course3',
  'course4',
  'course5',
  'course6',
  'course7',
  'course8',
  'course9',
  'course10',
  'course11',
  'course12',
  'course13',
  'course14',
  'course15',
  'palaceWing',
  'palaceMetal',
  'palaceVanish',
  'bowser1',
  'bowser2',
  'bowser3',
  'secretSlide',
  'secretWing',
  'secretAquarium',
  'endCake',
]

export const course = {
  castle:         'castle',
  course1:        'course1',
  course2:        'course2',
  course3:        'course3',
  course4:        'course4',
  course5:        'course5',
  course6:        'course6',
  course7:        'course7',
  course8:        'course8',
  course9:        'course9',
  course10:       'course10',
  course11:       'course11',
  course12:       'course12',
  course13:       'course13',
  course14:       'course14',
  course15:       'course15',
  palaceWing:     'palaceWing',
  palaceMetal:    'palaceMetal',
  palaceVanish:   'palaceVanish',
  bowser1:        'bowser1',
  bowser2:        'bowser2',
  bowser3:        'bowser3',
  secretSlide:    'secretSlide',
  secretWing:     'secretWing',
  secretAquarium: 'secretAquarium',
  endCake:        'endCake',
}

export type CourseIdentifier = 'castle' | 'course1' | 'course2' | 'course3' | 'course4' | 'course5' | 'course6' | 'course7' | 'course8' | 'course9' | 'course10' | 'course11' | 'course12' | 'course13' | 'course14' | 'course15' | 'palaceWing' | 'palaceMetal' | 'palaceVanish' | 'bowser1' | 'bowser2' | 'bowser3' | 'secretSlide' | 'secretWing' | 'secretAquarium' | 'endCake';
