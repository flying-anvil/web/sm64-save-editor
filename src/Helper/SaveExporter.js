// @flow

import type {
  GlobalDataType, HighscoresType, LevelDataType, LevelsDataType,
  SaveDataType,
  SaveFlagsType,
  SaveSlotType,
  SaveType
} from "../Data/Save";
import {downloadBlob} from "./Download";

const offset_global          = 0x01C0;
const offset_global_backup   = 0x01E0;
const offset_global_sound    = 0x0010;
const offset_global_language = 0x0012;
const offset_global_magic    = 0x001C;
const offset_global_checksum = 0x001E;

const offset_slot_a        = 0x0000;
const offset_slot_a_backup = 0x0038;
const offset_slot_b        = 0x0070;
const offset_slot_b_backup = 0x00A8;
const offset_slot_c        = 0x00E0;
const offset_slot_c_backup = 0x0118;
const offset_slot_d        = 0x0150;
const offset_slot_d_backup = 0x0188;

const offset_slot_flags1   = 0x0B;
const offset_slot_flags2   = 0x0A;
const offset_slot_flags3   = 0x09;
const offset_slot_magic    = 0x34;
const offset_slot_checksum = 0x36;
const offset_slot_level_castle         = 0x08;
const offset_slot_level_course1        = 0x0C;
const offset_slot_level_course2        = 0x0D;
const offset_slot_level_course3        = 0x0E;
const offset_slot_level_course4        = 0x0F;
const offset_slot_level_course5        = 0x10;
const offset_slot_level_course6        = 0x11;
const offset_slot_level_course7        = 0x12;
const offset_slot_level_course8        = 0x13;
const offset_slot_level_course9        = 0x14;
const offset_slot_level_course10       = 0x15;
const offset_slot_level_course11       = 0x16;
const offset_slot_level_course12       = 0x17;
const offset_slot_level_course13       = 0x18;
const offset_slot_level_course14       = 0x19;
const offset_slot_level_course15       = 0x1A;
const offset_slot_level_bowser1        = 0x1B;
const offset_slot_level_bowser2        = 0x1C;
const offset_slot_level_bowser3        = 0x1D;
const offset_slot_level_secretSlide    = 0x1E;
const offset_slot_level_palaceMetal    = 0x1F;
const offset_slot_level_palaceWing     = 0x20;
const offset_slot_level_palaceVanish   = 0x21;
const offset_slot_level_secretWing     = 0x22;
const offset_slot_level_secretAquarium = 0x23;
const offset_slot_level_endCake        = 0x24;

const magic_global = 0x4849;
const magic_slot   = 0x4441;

function calculateChecksum(from: number, length: number, data: Uint8Array): number {
  let sum = 0;

  const to = from + length;
  for (let i = from; i < to; i++) {
    sum += data[i];
  }

  return sum;
}


function applyGlobalData(globalData: GlobalDataType, offset: number, exportData: Uint8Array, view: DataView) {
  view.setUint16(offset + offset_global_sound, globalData.sound, false);
  view.setUint16(offset + offset_global_language, globalData.language, false);
  view.setUint16(offset + offset_global_magic, magic_global, false);

  const checksum = calculateChecksum(offset, 32, exportData);
  view.setUint16(offset + offset_global_checksum, checksum, false);
}

/**
 * bit1 is the rightmost, least signifikant bit
 */
function bitmapToByte(bit1: boolean, bit2: boolean, bit3: boolean, bit4: boolean, bit5: boolean, bit6: boolean, bit7: boolean, bit8: boolean): number {
  // noinspection JSBitwiseOperatorUsage
  return (
    (bit1 ? 0b00000001 : 0) |
    (bit2 ? 0b00000010 : 0) |
    (bit3 ? 0b00000100 : 0) |
    (bit4 ? 0b00001000 : 0) |
    (bit5 ? 0b00010000 : 0) |
    (bit6 ? 0b00100000 : 0) |
    (bit7 ? 0b01000000 : 0) |
    (bit8 ? 0b10000000 : 0)
  );
}

function applyFlags(flags: SaveFlagsType, offset: number, exportData: Uint8Array, view: DataView) {
  const flags1 = bitmapToByte(
    flags.fileInUse,
    flags.unlockedWingCap,
    flags.unlockedMetalCap,
    flags.unlockedVanishCap,
    flags.hasKeyBasement,
    flags.hasKeyUpstairs,
    flags.keyDoorUnlockedBasement,
    flags.keyDoorUnlockedUpstairs,
  );

  // console.log({
  //   map: flags1,
  //   fileInUse: flags.fileInUse,
  //   unlockedWingCap: flags.unlockedWingCap,
  //   unlockedMetalCap: flags.unlockedMetalCap,
  //   unlockedVanishCap: flags.unlockedVanishCap,
  //   hasKeyBasement: flags.hasKeyBasement,
  //   hasKeyUpstairs: flags.hasKeyUpstairs,
  //   keyDoorUnlockedBasement: flags.keyDoorUnlockedBasement,
  //   keyDoorUnlockedUpstairs: flags.keyDoorUnlockedUpstairs,
  // });

  const flags2 = bitmapToByte(
    flags.direDireDocksPortalMoved,
    flags.moatDrained,
    flags.starDoorUnlockedSecretSlide,
    flags.starDoorUnlockedWhompsFortress,
    flags.starDoorUnlockedCoolCoolMountain,
    flags.starDoorUnlockedJollyRogerBay,
    flags.starDoorUnlockedBowserDarkWorld,
    flags.starDoorUnlockedBowserFireSea,
  );

  const flags3 = bitmapToByte(
    flags.lostCapByLevelId,
    flags.lostCapInShiftingSandLand,
    flags.lostCapTallTallMountain,
    flags.lostCapSnowmansLand,
    flags.starDoorUnlocked3rdFloor,
    flags.unused1,
    flags.unused2,
    flags.unused3,
  );

  view.setUint8(offset + offset_slot_flags1, flags1, false);
  view.setUint8(offset + offset_slot_flags2, flags2, false);
  view.setUint8(offset + offset_slot_flags3, flags3, false);

  // TODO Level Data + Highscores

  view.setUint16(offset + offset_slot_magic, magic_slot, false);
}

function levelToByte(levelData: LevelDataType, nextLevel: ?LevelDataType = null) {
  return bitmapToByte(
    levelData.stars.star1,
    levelData.stars.star2,
    levelData.stars.star3,
    levelData.stars.star4,
    levelData.stars.star5,
    levelData.stars.star6,
    levelData.stars.starBonus,
    nextLevel && nextLevel.cannonUnlocked,
  );
}

function applyStars(levelsData: LevelsDataType, offset: number, exportData: Uint8Array, view: DataView) {
  // console.log(levelsData);

  const castle         = levelToByte(levelsData.castle, null);
  const course1        = levelToByte(levelsData.course1,        levelsData.course2);
  const course2        = levelToByte(levelsData.course2,        levelsData.course3);
  const course3        = levelToByte(levelsData.course3,        levelsData.course4);
  const course4        = levelToByte(levelsData.course4,        levelsData.course5);
  const course5        = levelToByte(levelsData.course5,        levelsData.course6);
  const course6        = levelToByte(levelsData.course6,        levelsData.course7);
  const course7        = levelToByte(levelsData.course7,        levelsData.course8);
  const course8        = levelToByte(levelsData.course8,        levelsData.course9);
  const course9        = levelToByte(levelsData.course9,        levelsData.course10);
  const course10       = levelToByte(levelsData.course10,       levelsData.course11);
  const course11       = levelToByte(levelsData.course11,       levelsData.course12);
  const course12       = levelToByte(levelsData.course12,       levelsData.course13);
  const course13       = levelToByte(levelsData.course13,       levelsData.course14);
  const course14       = levelToByte(levelsData.course14,       levelsData.course15);
  const course15       = levelToByte(levelsData.course15,       levelsData.bowser1);
  const bowser1        = levelToByte(levelsData.bowser1,        levelsData.bowser2);
  const bowser2        = levelToByte(levelsData.bowser2,        levelsData.bowser3);
  const bowser3        = levelToByte(levelsData.bowser3,        levelsData.secretSlide);
  const secretSlide    = levelToByte(levelsData.secretSlide,    levelsData.palaceMetal);
  const palaceMetal    = levelToByte(levelsData.palaceMetal,    levelsData.palaceWing);
  const palaceWing     = levelToByte(levelsData.palaceWing,    levelsData.palaceVanish);
  const palaceVanish   = levelToByte(levelsData.palaceVanish,    levelsData.secretWing);
  const secretWing     = levelToByte(levelsData.secretWing,     levelsData.secretAquarium);
  const secretAquarium = levelToByte(levelsData.secretAquarium, levelsData.endCake);
  const endCake        = levelToByte(levelsData.endCake, null);

  view.setUint8(offset + offset_slot_level_castle, castle)
  view.setUint8(offset + offset_slot_level_course1, course1)
  view.setUint8(offset + offset_slot_level_course2, course2)
  view.setUint8(offset + offset_slot_level_course3, course3)
  view.setUint8(offset + offset_slot_level_course4, course4)
  view.setUint8(offset + offset_slot_level_course5, course5)
  view.setUint8(offset + offset_slot_level_course6, course6)
  view.setUint8(offset + offset_slot_level_course7, course7)
  view.setUint8(offset + offset_slot_level_course8, course8)
  view.setUint8(offset + offset_slot_level_course9, course9)
  view.setUint8(offset + offset_slot_level_course10, course10)
  view.setUint8(offset + offset_slot_level_course11, course11)
  view.setUint8(offset + offset_slot_level_course12, course12)
  view.setUint8(offset + offset_slot_level_course13, course13)
  view.setUint8(offset + offset_slot_level_course14, course14)
  view.setUint8(offset + offset_slot_level_course15, course15)
  view.setUint8(offset + offset_slot_level_bowser1, bowser1)
  view.setUint8(offset + offset_slot_level_bowser2, bowser2)
  view.setUint8(offset + offset_slot_level_bowser3, bowser3)
  view.setUint8(offset + offset_slot_level_secretSlide, secretSlide)
  view.setUint8(offset + offset_slot_level_palaceMetal, palaceMetal)
  view.setUint8(offset + offset_slot_level_palaceWing, palaceWing)
  view.setUint8(offset + offset_slot_level_palaceVanish, palaceVanish)
  view.setUint8(offset + offset_slot_level_secretWing, secretWing)
  view.setUint8(offset + offset_slot_level_secretAquarium, secretAquarium)
  view.setUint8(offset + offset_slot_level_endCake, endCake)
}

function applyHighScores(highscores: HighscoresType, offset: number, exportData: Uint8Array, view: DataView) {
  for (let i = 0x25; i <= 0x33; i++) {
    view.setUint8(offset + i, highscores[`course${i - 0x24}`] || 0, false)
  }
}

function applySaveSlot(slotData: SaveSlotType, offset: number, exportData: Uint8Array, view: DataView) {
  applyFlags(slotData.flags, offset, exportData, view);
  applyStars(slotData.levelData, offset, exportData, view);
  applyHighScores(slotData.highscores, offset, exportData, view);

  view.setUint16(offset + offset_slot_checksum, calculateChecksum(offset, 56, exportData), false)
}

function applySaveData(saveData: SaveDataType, exportData: Uint8Array, view: DataView) {
  applySaveSlot(saveData.a, offset_slot_a, exportData, view);
  applySaveSlot(saveData.a, offset_slot_a_backup, exportData, view);
  applySaveSlot(saveData.b, offset_slot_b, exportData, view);
  applySaveSlot(saveData.b, offset_slot_b_backup, exportData, view);
  applySaveSlot(saveData.c, offset_slot_c, exportData, view);
  applySaveSlot(saveData.c, offset_slot_c_backup, exportData, view);
  applySaveSlot(saveData.d, offset_slot_d, exportData, view);
  applySaveSlot(saveData.d, offset_slot_d_backup, exportData, view);
}

export function exportSave(saveData: SaveType, download: boolean = false) {
  console.log(saveData);

  const exportData   = new Uint8Array(512);
  const exportBuffer = exportData.buffer;
  const exportView   = new DataView(exportBuffer);

  applyGlobalData(saveData.globalData, offset_global, exportData, exportView);
  applyGlobalData(saveData.globalData, offset_global_backup, exportData, exportView);

  applySaveData(saveData.saveData, exportData, exportView);
  console.log(exportData);

  if (download) {
    downloadBlob(new Blob([exportBuffer]), 'sm64-export.eep',);
  }
}
