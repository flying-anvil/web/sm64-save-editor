// @flow

export function downloadData(data: string|Uint8Array, fileName: string, fileType: string = 'application/json') {
  const blob = new Blob([data], {type: fileType});
  downloadBlob(blob, fileName, fileType);
}

export function downloadBlob(blob: Blob, fileName: string, fileType: string = 'application/octet-stream') {
  const a = document.createElement('a');
  a.download = fileName;
  a.href = URL.createObjectURL(blob);
  a.dataset.downloadurl = [fileType, a.download, a.href].join(':');
  a.style.display = 'none';

  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);

  setTimeout(
    () => URL.revokeObjectURL(a.href),
    1500,
  );
}
